import { Request, Response, NextFunction } from 'express'
import { CustomError } from '../errorHandling/index'

export function errorHandler(err: Error, req: Request, res: Response, next: NextFunction) {

  if (err instanceof CustomError) {
    return res.status(err.getStatusCode()).send({ errors: err.serializeErrors() })
  }
  return res.status(400).send({ errors: { message: 'something went wrong'}})
}