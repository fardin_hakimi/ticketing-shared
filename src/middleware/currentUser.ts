import { Request, Response, NextFunction } from 'express'
import { NotAuthorizedError } from '../errorHandling'
import jwt from 'jsonwebtoken'

export interface UserPayload {
  id: string
  email: string
}

declare global {
  namespace Express {
    interface Request {
      currentUser?: UserPayload
      session?: {
        [key: string]: any
      }
    }
  }
}

export function authorizeRequest(req: Request, res: Response, next: NextFunction) {
  if(!req.currentUser) throw new NotAuthorizedError()
  next()
}

export function currentUser(req: Request, res: Response, next: NextFunction) {

  const userJwt: string | undefined = req.session && req.session.jwt
  
  if (!userJwt) return next()

  try {
    const payload = jwt.verify(userJwt, process.env.JWT_KEY!) as UserPayload
    req.currentUser = payload
  } catch (error) {}

  next()
}
