export interface NormalizedErrorItem {
  message: string
  field?: string
}

export abstract class CustomError extends Error {
  private statusCode: number
  constructor(statusCode: number) {
    super()
    this.statusCode = statusCode
    Object.setPrototypeOf(this, CustomError.prototype)
  }
  abstract serializeErrors(): NormalizedErrorItem[]
  getStatusCode(): number {
    return this.statusCode
  }
}
export class ValidationError extends CustomError {
  private errors: any[] = []
  constructor(errors: any[]) {
    super(400)
    this.errors = errors
    Object.setPrototypeOf(this, ValidationError.prototype)
  }
  public serializeErrors() {
    return this.errors.map( ( { msg, param}) => {
      return { message: msg, field: param}
    })
  }
}

export class DatbaseConnectionError extends CustomError {
  private error = 'Failed to connect to the database'
  constructor() {
    super(500)
    Object.setPrototypeOf(this, DatbaseConnectionError.prototype)
  }
  public serializeErrors() {
    return [ { message: this.error, }]
  }
}


export class NotFoundError extends CustomError {

  constructor() {
    super(404)
    Object.setPrototypeOf(this, NotFoundError.prototype)
  }

  public serializeErrors() {
    return [ { message: 'Not found'}]
  }
}

export class BadRequestError extends CustomError {
  public message: string
  constructor(message: string) {
    super(400)
    this.message = message
    Object.setPrototypeOf(this, BadRequestError.prototype)
  }

  public serializeErrors() {
    return [{ message: this.message }]
  }
}

export class NotAuthorizedError extends CustomError {
  public message: string
  constructor(message = 'Not authorized') {
    super(401)
    this.message = message
    Object.setPrototypeOf(this, NotAuthorizedError.prototype)
  }

  public serializeErrors() {
    return [{ message: this.message }]
  }
}